//package test.java;

import main.java.vn.onepay.ows.Authorization;
import main.java.vn.onepay.tokencvv.TokenCvvGenerator;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;


/**
 * Created by locdt on 4/17/16
 * Updated by thuanmd on 6/13/2016
 */
public class Test {
    private static final String RESPONSE_CODE = "response_code";
    private static final String HEADER = "header";
    private static final String BODY = "body";
    private static final String BASE_URL = "https://mtf.onepay.vn"; // Test domain
    //private static final String BASE_PATH = "/tsp/api/v1";
    private static final String accessKeyId = "TSPTEST"; // Access ID, provide by OnePAY ADAYROI
    private static final String secretAccessKey = "KEY_cGwNtNm38VwT6WifCFZwtw"; // Access Key, provide by OnePAY KEY_HDF5v_314KWNkSla9vSkEw
    private static final String region = "onepay"; //fixed
    private static final String service = "tsp"; //fixed

    private static final DateFormat yyyyMMddThhmmssZ = new SimpleDateFormat("yyyyMMdd'T'hhmmss'Z'");

    public static void main(String... args) throws Exception {

        System.out.println("---------------------kaka---------------------------------------------------");
        System.out.println("hello world".getBytes("UTF-8"));
        System.out.println(Arrays.toString("hello world".getBytes("UTF-8")));
        System.out.println("---------------------haha---------------------------------------------------");
        Test client = new Test();
        client.createUser();
        //client.getUser();
        //client.searchUser();
        //client.createInstruments();
        //client.createPayments();



    }


    // Create User
    public void createUser() throws Exception {
        String uri = "/tsp/api/v1/users";
        String method = "POST";
        String body = ("{" +
                "  \"group_id\":\"tsptest\"," +
                "  \"ref_id\":\"THUANMD667\"," +
                "  \"first_name\":\"Nguyen\"," +
                "  \"last_name\":\"Van A\"," +
                "  \"mobile\":\"0973830045\"," +
                "  \"email\":\"nguyenvana@gmail.com\"," +
                "  \"address\":{" +
                "    \"line1\":\"194 Tran Quang Khai\"," +
                "    \"state\":\"Hoan Kiem\"," +
                "    \"city\":\"Ha Noi\"," +
                "    \"country_code\":\"VN\"" +
                "  }"+
                "}");

        request(method, uri, body);
    }
    //test: response_body={"id":"USR_fXtk1G6fVWvsjE7qn3gwNA","ref_id":"THUANMD123","group_id":"adayroi","first_name":"Nguyen","last_name":"Van A","mobile":"","email":"nguyenvana@gmail.com","address":{"line1":"194 Tran Quang Khai","line2":"","state":"Hoan Kiem","city":"Ha Noi","postal_code":"","country_code":"VN"},"state":"approved"}
    //pro: response_body={"id":"USR_q0Pbtu3x0XhdLKUJwytMqg","ref_id":"THUANMD12345","group_id":"adayroi","first_name":"Nguyen","last_name":"Van A","mobile":"","email":"nguyenvana@gmail.com","address":{"line1":"194 Tran Quang Khai","line2":"","state":"Hoan Kiem","city":"Ha Noi","postal_code":"","country_code":"VN"},"state":"approved"}
    public void searchUser() throws Exception {
        String uri = "/tsp/api/v1/users";
        String method = "GET";
        String body = null;
        request(method, uri, body);
    }

    public void getUser() throws Exception {
        String uri = "/tsp/api/v2/users/USR_k5o9uSnzwjJLQiDj3-EDFg";
        String method = "GET";
        String body = null;
        request(method, uri, body);
    }


    public void createInstruments() throws Exception {
        String uri = "/tsp/api/v1/instruments";
        String method = "POST";
        String body = ("{" +
                "  \"user_id\":\"USR_hEZVNf7SsrIl1qMnwIuNdQ\"," + //THUANMD666
                "  \"transaction\":{" +
                "    \"psp_id\":\"I\"," +
                "    \"merchant_id\":\"TESTONEPAY\"," +
                "    \"merchant_txn_ref\":\"TEST_1497160679035442183772\"" +
                "  },"+
                "  \"create_token\":true" +
                "}");

        request(method, uri, body);
    }
    //test 3D: response_body={"id":"INS_Lfm2SOjUbAu0Fbn5P0Cmsg","ref_id":"INS_k0rLtozt-DtrTZDbPIlj8g","user_id":"USR_fXtk1G6fVWvsjE7qn3gwNA","type":"card","name":"","number":"455701xxxxxxxxxx902","month":"05","year":"2017","issuer":{"swift_code":"","name":"","brand":{"id":"visa","name":"Visa","version":""}},"billing_address":{"line1":"","line2":"","state":"","city":"","postal_code":"","country_code":""},"create_time":"20160930T104755Z","update_time":"20160930T104755Z","state":"approved","token":{"id":"TKN_r1q-9LObgKt-bvoHubF69w","instrument_id":"INS_k0rLtozt-DtrTZDbPIlj8g","number":"4557017706140886902","icvv":"CVV_i0Utur_ZQY_2I-f0dqYJtA","expire_month":"05","expire_year":2017,"state":"approved"}}
    //Pro: response_body={"id":"INS_rJxCUKZBh2MeGoCZtwG3RQ","ref_id":"INS_mv2S4UOwKGlPb8YI_GA7nQ","user_id":"USR_q0Pbtu3x0XhdLKUJwytMqg","type":"card","name":"","number":"400555xxxxxxxxxx001","month":"05","year":"2017","issuer":{"swift_code":"","name":"","brand":{"id":"visa","name":"Visa","version":""}},"billing_address":{"line1":"","line2":"","state":"","city":"","postal_code":"","country_code":""},"create_time":"20161005T104407Z","update_time":"20161005T104407Z","state":"approved","token":{"id":"TKN_FZJJthkUR6p8mdp4qhFJSA","instrument_id":"INS_mv2S4UOwKGlPb8YI_GA7nQ","number":"4005552024842674001","icvv":"CVV_KJuG9o1FL0_jekO5d4PD0Q","expire_month":"05","expire_year":2017,"state":"approved"}}





    public void createPayments() throws Exception {
        String uri = "/tsp/api/v1/payments";
        String method = "POST";

        String tokenNumber = "4005556028681151001"; // 2D: , 3D: 5313583340080104430 fraud 4557015581121118902
        int expireMonth = 05;
        int expireYear = 2018;
        String initCVV = "CVV_U8pbV8jXz_kNfEL2EoLEXQ"; // 2D: , 3D: CVV_gGyyfKhTkHbpx8StqVuZQg fraud CVV_I_IoKDT4JqFp0XIiu7Dq1w
        int sequenceNumber = 123;
        String payTime = "20170611T112612Z";


        TokenCvvGenerator dcvv = new TokenCvvGenerator();
        String tcvv = dcvv.generate(tokenNumber, expireMonth, expireYear, initCVV, sequenceNumber, payTime);

        int cvv = Integer.parseInt(tcvv);

        //System.out.println("Token_CVV: " + cvv);

        //int cvv = 8668;

        String merchantTxnRef = yyyyMMddThhmmssZ.format(new Date());

        String body = ("{" +
                "  \"merchant_id\":\"TESTONEPAY\"," +
                "  \"merchant_txn_ref\":\"OP_TK_REF_" + merchantTxnRef + "\"," +
                //"  \"merchant_txn_ref\":\"OP_TK_REF_" + "123456" + "\"," +
                "  \"user_id\":\"USR_hEZVNf7SsrIl1qMnwIuNdQ\"," +
                "  \"amount\":1000," +
                "  \"currency\":\"VND" + "\"," +
                "  \"order\":{" +
                "    \"id\":\"ADR_TK_ORD_" + merchantTxnRef + "\"," +
                "    \"information\":\"ADR_TK_ORD_" + merchantTxnRef + "\"" +
                "  },"+
                "  \"token\":{" +
                "    \"number\":\"" + tokenNumber + "\"," +
                "    \"expire_month\":" + expireMonth + "," +
                "    \"expire_year\":" + expireYear + "," +
                "    \"cvv\":" + cvv + "," + // dynamic CVV of Token, do not of Card
                "    \"pay_time\":\"" + payTime + "\"," +
                "    \"sequence_number\":" + sequenceNumber + "" +
                "  },"+
                "  \"terminal\":{" +
                "    \"id\":\"TER_ABCD1234\"," +
                "    \"type\":\"ECOM_WEB\"," +
                "    \"ip\":\"127.0.0.1\"," +
                "    \"location\":\"HN\"" +
                "  },"+
                "  \"return_url\":\"https://mtf.onepay.vn/client/qt/dr/?mode=TEST\"," +
                "  \"cancel_url\":\"https://onepay.vn\"" +
                "}");

        request(method, uri, body);

    }
    //response body={"id":"PAY_AKompDlFTxZkwbN6LpKDbQ","channel_id":"ADAYROI","channel_payment_id":"ADR_TK_REF_12345","group_id":"adayroi","merchant_id":"TESTONEPAY","merchant_name":"","merchant_txn_ref":"ADR_TK_REF_12345","amount":0.0,"currency":"","method":"","order":{"id":"ORD_Lz-RA8DTEs4owzDrEgQZ3Q","information":"TEST TSP PAYMENT"},"instrument":{"type":"card","name":"","number":"400555xxxxxxx001"},"terminal":{"id":"TER_ABCD1234","type":"ECOM_WEB","ip":"127.0.0.1","location":"HN"},"state":"more_info_required","pay_time":"20160612T214950Z","return_url":"http://onepay.vn/testonepay/payments","cancel_url":"http://onepay.vn/testonepay/payments","links":{"information_update":{"href":"https://mtf.onepay.vn/payment/payments/PAY_AKompDlFTxZkwbN6LpKDbQ?X-OP-Algorithm=OWS1-HMAC-SHA256&X-OP-Credential=TSP_WEB_CLIENT%2F20160613%2Fonepay%2Ftspwebclient%2Fows1_request&X-OP-Date=20160613T044950Z&X-OP-Expires=360&X-OP-SignedHeaders=&X-OP-Signature=445ce5afd9c20a6576a60f57f4f19fe024db0b16dc32140d34e74e2e964a6e0a","method":"REDIRECT"}}}



    private void request(String method, String uri, String body) throws Exception {


        Map<String, String> queryParams = new LinkedHashMap<String, String>();
        // if use searchUser
        //queryParams.put("group", "tsptest");
        //queryParams.put("reference", "THUANMD666");
        // queryParams.put("param2", "Giá trị tham số 2");


        Map<String, String> signedHeaders = new LinkedHashMap<String, String>();
        signedHeaders.put("Content-Type", "application/json");
        signedHeaders.put("Accept", "application/json");
        signedHeaders.put("Accept-Language", "vi");
//        signedHeaders.put(Authorization.X_OP_DATE_HEADER, "20170630T045904Z");
        signedHeaders.put(Authorization.X_OP_DATE_HEADER, "20171102T142037Z");
        signedHeaders.put(Authorization.X_OP_EXPIRES_HEADER, "900");


        //Date timeStamp = new Date();
        //int expires = 900;
        byte[] bd = null == body ? "".getBytes("UTF-8") : body.getBytes("UTF-8");
        //byte[] bd = new byte[] { };
        //Generate Authorization header
        Authorization auth = new Authorization(accessKeyId, secretAccessKey, region, service, method, uri, queryParams,
                signedHeaders,bd);



        String queryString = "?";
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            queryString += entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8") + "&";
        }
        queryString = queryString.substring(0, queryString.length() - 1);

        System.out.println("HTTP RAW REQUEST:");
        System.out.println(method + " " + uri + queryString + " HTTP/1.1");
        System.out.println("Host: mtf.onepay.vn");
        for (Map.Entry<String, String> entry : signedHeaders.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println("X-OP-Authorization: " + auth.toString());//Required
        System.out.println("X-OP-Date: " + auth.getTimeStampString());//Required
        System.out.println("X-OP-Expires: " + auth.getExpires());//Required
        //System.out.println("Content-Length: " + body.length());
        System.out.println("");
        System.out.println(body);

        //System.out.println("* Auth Debug Info: " + auth.getDebugInfo());

        // HEADER
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Accept-Language", "vi");
        headers.put("X-OP-Authorization", auth.toString());//Required
        headers.put("X-OP-Date", auth.getTimeStampString());//Required

        headers.put("X-OP-Expires", "900");//Required
        //headers.put("X-OP-Expires", auth.getExpires());//Required
        //headers.put("Content-Length", content.length());

        //System.out.println(auth.getDebugInfo().toString());
        // REQUEST
        httpRequest(BASE_URL + uri+ queryString, method, headers,bd);

    }

    /*
    private Map post(String url, Map<String, String> headers, byte[] body) throws Exception {
        return request(url, "POST", headers, body);
    } */

    private Map httpRequest(String url, String method, Map<String, String> headers, byte[] body) throws Exception {


        Map<String, Object> result = new HashMap<String, Object>();
        System.out.println("request: url=" + url + ", method=" + method + ", params=" + body);
        //System.out.println("request: url=" + url + "?group=adayroi&reference=THUANMD123, method=" + method + ", params=" + body);
        //HttpsURLConnection conn = (HttpsURLConnection) (new URL(url)).openConnection();
        //HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
        URL obj = new URL(url);
        HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();

        conn.setRequestMethod(method);

        if (headers != null) {
            for (String key : headers.keySet()) {
                conn.setRequestProperty(key, headers.get(key));
            }
        }
        if (method.matches("^(POST|PUT)$")) {
            conn.setRequestProperty("Content-Length", String.valueOf(body != null ? body.length : "0"));
        }
        conn.setUseCaches(false);


        //conn.setDoInput(method.matches("^(POST|PUT)$"));
        conn.setDoInput(true);
        //conn.setDoOutput(false);
        conn.setInstanceFollowRedirects(false);


        // Send request params
        if (body != null && body.length>0) {
            conn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.write(body);
            wr.flush();
            wr.close();
        } else {
            conn.setDoOutput(false);
        }


        int responseCode = conn.getResponseCode();

        String responseDesc = conn.getResponseMessage();

        System.out.println("response_desc=" + responseDesc);

        result.put(RESPONSE_CODE, responseCode);
        System.out.println("response_code=" + responseCode);
        //Get response header
        Map<String, List<String>> resHeaders = conn.getHeaderFields();
        result.put(HEADER, resHeaders);
        System.out.println("response_headers=" + resHeaders);

        byte[] content = null;
        //Get response content
        if (conn.getDoInput()) {
            InputStream is;
            if (responseCode >= 400) {
                is = conn.getErrorStream();
            } else {
                is = conn.getInputStream();
            }
            System.out.println("is=" + is);
            if (is != null) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                byte[] buf = new byte[1000];
                int length;
                while ((length = is.read(buf)) != -1) {
                    bout.write(buf, 0, length);
                }
                bout.flush();
                content = bout.toByteArray();
                bout.close();

            }
        }
        result.put(BODY, content);
        System.out.println("response_body=" + (content != null ? new String(content, "UTF-8") : null));
        conn.disconnect();

        return result;
    }

}
